package com.jesusceladev.notes.data

import com.jesusceladev.notes.domain.Note
import kotlinx.coroutines.flow.Flow

class NoteRepository(private val notePersistenceDataSource: NotePersistenceDataSource) {

    fun saveNote(note: Note): Int {
        return notePersistenceDataSource.saveNote(note)
    }

    fun updateNote(note: Note): Int {
        return notePersistenceDataSource.updateNote(note)
    }

    fun existNote(uid: Int): Boolean {
        return notePersistenceDataSource.existNote(uid)
    }

    fun getNotes(): List<Note> {
        return notePersistenceDataSource.getNotes()
    }

    fun observeNotes(): Flow<List<Note>> {
        return notePersistenceDataSource.observeNotes()
    }

    fun getNote(uid: Int): Note {
        return notePersistenceDataSource.getNote(uid)
    }

    fun deleteNote(uid: Int): Int {
        return notePersistenceDataSource.deleteNote(uid)
    }

    interface NotePersistenceDataSource {
        fun saveNote(note: Note): Int
        fun updateNote(note: Note): Int
        fun existNote(uid: Int): Boolean
        fun getNotes(): List<Note>
        fun observeNotes(): Flow<List<Note>>
        fun getNote(uid: Int): Note
        fun deleteNote(uid: Int): Int
    }
}