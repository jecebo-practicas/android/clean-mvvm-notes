package com.jesusceladev.notes.domain

enum class NoteType {
    TEXT, LIST, CHECK_LIST
}