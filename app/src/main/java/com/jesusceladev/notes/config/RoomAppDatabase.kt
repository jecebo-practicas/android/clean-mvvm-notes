package com.jesusceladev.notes.config

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.jesusceladev.notes.datasource.NoteRoomDAO
import com.jesusceladev.notes.datasource.entity.NoteVO
import com.jesusceladev.notes.utils.Converters

@Database(entities = [NoteVO::class], version = 1)
@TypeConverters(Converters::class)
abstract class RoomAppDatabase : RoomDatabase() {

    abstract fun noteDao(): NoteRoomDAO

}