package com.jesusceladev.notes.config

import androidx.room.Room
import com.jesusceladev.notes.datasource.NoteRoomDataSourceImpl
import com.jesusceladev.notes.data.NoteRepository
import com.jesusceladev.notes.usecases.DeleteNote
import com.jesusceladev.notes.usecases.GetNote
import com.jesusceladev.notes.usecases.GetNotes
import com.jesusceladev.notes.usecases.SaveOrUpdateNote
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val dataModule = module {
    factory { NoteRepository(get()) }
}

val useCasesModule = module {
    factory { GetNotes(get()) }
    factory { GetNote(get()) }
    factory { DeleteNote(get()) }
    factory { SaveOrUpdateNote(get()) }
}

val appModule = module {
    single<NoteRepository.NotePersistenceDataSource> { NoteRoomDataSourceImpl(get()) }
    single {
        Room.databaseBuilder(androidApplication(), RoomAppDatabase::class.java, "notes-db")
            .enableMultiInstanceInvalidation().build()
    }
    single { get<RoomAppDatabase>().noteDao() }
}