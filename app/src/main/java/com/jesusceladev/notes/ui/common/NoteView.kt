package com.jesusceladev.notes.ui.common

import android.text.format.DateUtils
import com.jesusceladev.notes.domain.NoteType
import com.jesusceladev.notes.utils.toDate
import java.io.Serializable
import java.util.*

data class NoteView(
    val uid: Int? = null,
    var name: String = "",
    var description: String? = "",
    val type: String = NoteType.TEXT.name,
    val createdDate: String? = "",
    val createdTime: String? = "",
    var modifiedDate: String? = "",
    var modifiedTime: String? = ""
) : Serializable {

    fun getModifiedDateOrTime(): String? {
        return this.modifiedDate?.let {
            val modifiedDateTime = (this.modifiedDate + " " + this.modifiedTime).toDate()
            return modifiedDateTime?.let {
                if (DateUtils.isToday(it.time)) this.modifiedTime else this.modifiedDate
            }
        }
    }
}

