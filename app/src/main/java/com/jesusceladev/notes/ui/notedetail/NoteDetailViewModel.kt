package com.jesusceladev.notes.ui.notedetail

import android.text.format.DateUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jesusceladev.notes.ui.common.NoteChangeEvent
import com.jesusceladev.notes.ui.common.NoteChangeType
import com.jesusceladev.notes.ui.common.NoteView
import com.jesusceladev.notes.utils.toNote
import com.jesusceladev.notes.utils.toNoteView
import com.jesusceladev.notes.usecases.DeleteNote
import com.jesusceladev.notes.usecases.GetNote
import com.jesusceladev.notes.usecases.SaveOrUpdateNote
import com.jesusceladev.notes.utils.toDate
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*

class NoteDetailViewModel : ViewModel(), KoinComponent {

    private val _note = MutableLiveData<NoteView>()
    val note: LiveData<NoteView> get() = _note

    private val _noteChange = MutableLiveData<NoteChangeEvent>()
    val noteChange: LiveData<NoteChangeEvent> get() = _noteChange

    private val _progressVisible = MutableLiveData<Boolean>()
    val progressVisible: LiveData<Boolean> get() = _progressVisible

    private val saveOrUpdateNote: SaveOrUpdateNote by inject()
    private val deleteNote: DeleteNote by inject()
    private val getNote: GetNote by inject()

    fun setNoteView(note: NoteView?) {
        _note.value = note ?: NoteView()
    }

    fun getNoteView(): NoteView {
        return _note.value ?: NoteView()
    }

    fun createOrUpdateNote() {
        viewModelScope.launch {
            _progressVisible.value = true
            val noteCreated = saveOrUpdateNote.run(_note.value!!.toNote())
            if (noteCreated > 0) {
                _note.value =
                    getNote.run(_note.value!!.uid?.let { _note.value!!.uid } ?: noteCreated)
                        .toNoteView()
                _noteChange.value = NoteChangeEvent(_note.value!!, NoteChangeType.CREATE)
            }
            _progressVisible.value = false
        }
    }

    fun deleteNote() {
        viewModelScope.launch {
            val notesDeletedCount = deleteNote.run(_note.value?.uid!!)
            if (notesDeletedCount > 0) _noteChange.value =
                NoteChangeEvent(_note.value!!, NoteChangeType.DELETE)
        }
    }
}