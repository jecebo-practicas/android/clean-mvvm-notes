package com.jesusceladev.notes.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jesusceladev.notes.R
import com.jesusceladev.notes.ui.common.NoteView
import com.jesusceladev.notes.utils.inflate
import kotlinx.android.synthetic.main.adapter_note_item.view.*

class NoteItemAdapter(
    private var noteList: List<NoteView>,
    private val listener: (NoteView) -> Unit,
    private val longListener: (NoteView) -> Boolean
) :

    RecyclerView.Adapter<NoteItemAdapter.NoteViewHolder>() {

    class NoteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(note: NoteView) {
            itemView.note_name.text = note.name
            itemView.note_description.text = note.description
            itemView.note_date.text = note.modifiedDate.toString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        return NoteViewHolder(parent.inflate(R.layout.adapter_note_item))
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val item = noteList[position]
        holder.bind(item)
        holder.itemView.setOnClickListener { listener(item) }
        holder.itemView.setOnLongClickListener { longListener(item) }
    }

    override fun getItemCount(): Int = noteList.size

    fun updateNoteList(noteList: List<NoteView>) { this.noteList = noteList}

}