package com.jesusceladev.notes.ui.notelist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jesusceladev.notes.ui.common.NoteChangeEvent
import com.jesusceladev.notes.ui.common.NoteChangeType
import com.jesusceladev.notes.ui.common.NoteView
import com.jesusceladev.notes.utils.toNoteView
import com.jesusceladev.notes.usecases.DeleteNote
import com.jesusceladev.notes.usecases.GetNotes
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject

class NoteListViewModel() : ViewModel(), KoinComponent {

    private val _noteList = MutableLiveData<List<NoteView>>()
    val noteList: LiveData<List<NoteView>> get() = _noteList

    private val _noteChange = MutableLiveData<NoteChangeEvent>()
    val noteChange: LiveData<NoteChangeEvent> get() = _noteChange

    private val getNotes: GetNotes by inject()
    private val deleteNote: DeleteNote by inject()

    fun getNotes() {
        viewModelScope.launch {
            val notes = getNotes.run()
            _noteList.value = notes.map { it.toNoteView() }
        }
    }

    fun observeNotes() {
        viewModelScope.launch {
            getNotes.runObserve().collect {
                _noteList.value = it.map { it.toNoteView() }
            }
        }
    }

    fun deleteNote(note: NoteView) {
        viewModelScope.launch {
            val notesDeletedCount = deleteNote.run(note.uid!!)
            if (notesDeletedCount > 0) _noteChange.value = NoteChangeEvent(note, NoteChangeType.DELETE)
        }
    }

    fun setChangeEvent(change: NoteChangeEvent) {
        _noteChange.value = change
    }
}
