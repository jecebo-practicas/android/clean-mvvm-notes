package com.jesusceladev.notes.ui.common

enum class NoteChangeType {
    CREATE, DELETE
}
