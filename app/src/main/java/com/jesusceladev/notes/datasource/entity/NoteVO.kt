package com.jesusceladev.notes.datasource.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "notes")
data class NoteVO(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,
    val name: String,
    val description: String?,
    val type: String,
    @ColumnInfo(name = "created_date")
    val createdDate: Date?,
    @ColumnInfo(name = "modified_date")
    val modifiedDate: Date?
)
