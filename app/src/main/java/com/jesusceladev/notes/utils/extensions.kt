package com.jesusceladev.notes.utils

import android.app.Activity
import android.content.Context
import android.content.res.Configuration
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.BindingAdapter
import com.google.android.material.snackbar.Snackbar
import com.jesusceladev.notes.R


fun ViewGroup.inflate(layout: Int): View {
    return LayoutInflater.from(context).inflate(layout, this, false)
}

@BindingAdapter("visible")
fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun Context.toast(message: String, duration: Int? = 0) {
    Toast.makeText(this, message, duration ?: Toast.LENGTH_LONG).show()
}

fun View.snackBar(message: String, duration: Int? = 2000) {
    Snackbar.make(this, message.subSequence(0, message.lastIndex), duration!!).show()
}

fun Context.confirmDialog(
    title: String, message: String, confirm: String,
    cancel: String? = getString(R.string.dialog_text_cancelar),
    confirmAction: () -> Unit
) {
    val builder = AlertDialog.Builder(this)
    builder.apply {
        setTitle(title)
        setMessage(message)
        setPositiveButton(confirm) { _, _ -> confirmAction.invoke() }
        setNegativeButton(cancel) { dialog, _ -> dialog.cancel() }
    }
    builder.create().show()
}

fun Activity.hideKeyboard() {
   this.currentFocus?.let {
       val imm: InputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
       imm.hideSoftInputFromWindow(it.windowToken, 0)
   }
}
fun Configuration.isOrientationVertical(): Boolean {
    return orientation == Configuration.ORIENTATION_PORTRAIT
}