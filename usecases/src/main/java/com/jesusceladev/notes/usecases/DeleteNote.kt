package com.jesusceladev.notes.usecases

import com.jesusceladev.notes.data.NoteRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DeleteNote(private val noteRepository: NoteRepository) {

    suspend fun run(uid: Int): Int {
        return withContext(Dispatchers.IO) { noteRepository.deleteNote(uid) }
    }
}