package com.jesusceladev.notes.usecases

import com.jesusceladev.notes.data.NoteRepository
import com.jesusceladev.notes.domain.Note
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetNote(private val noteRepository: NoteRepository) {

    suspend fun run(uid: Int): Note {
        return withContext(Dispatchers.IO) { noteRepository.getNote(uid) }
    }
}